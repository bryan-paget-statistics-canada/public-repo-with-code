import git
import tempfile
import pandas as pd

with tempfile.TemporaryDirectory() as tmpdirname:
    print('created temporary directory', tmpdirname)
    git.Repo.clone_from(
        "https://gitlab.com/bryan-paget-statistics-canada/private-repo-with-data",
        tmpdirname)

    df = pd.read_csv(f"{tempdirname/users.csv}")

    df.head()
